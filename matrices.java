package ZAD6;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.NaN;

public class Matrices {

    /**
     * Liczenie wyznacznika macierzy
     *
     * @param matrix Macierz do obliczen
     * @return obliczony wyznacznik lub NaN jesli macierz nie jest kwadratowa
     */
    public static double determinant(double[][] matrix) {

        //Jesli macierz nie jest kwadratowa zwraca NaN
        if (!isSquareMatrix(matrix)) {
            return NaN;
        }

        int size = matrixHeight(matrix);

        //Jesli macierz jest wieksza niz 3, liczy wyznacznik z rozwiniecia la place'a
        if (size > 3) {
            return Laplace(matrix);
        }


        //Macierz 2x2
        if (size == 2) {
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        }

        //Macierz jednoelementowa
        if (size == 1) {
            return matrix[0][0];
        }

        //Gdy rozmiar macierzy wynosi dokladnie 3

        //Lewa czesc metody Sarrusa
        double left = 0;

        for (int n = 0; n < size; n++) {
            double temp = 1;
            for (int i = 0; i < size; i++) {
                temp *= matrix[(i + n) % size][i % size];
            }
            left += temp;
        }

        //Prawa czesc metody Sarrusa
        double right = 0;

        for (int n = 0; n < size; n++) {
            double temp = 1;
            for (int i = 0; i < size; i++) {
                temp *= matrix[(i) % size][(size - 1 - i - n + size) % size];
            }
            right += temp;
        }

        //Zgodnie z metoda Sarrusa odejmowanie jednego wyniku od drugiego
        return left - right;
    }

    /**
     * Funkcja liczaca wyznacznik podanej macierzy poprzez rozwiniecie LaPlace'a
     *
     * @param matrix macierz do obliczen
     * @return wyznacnzik macierzy
     */
    private static double Laplace(double[][] matrix) {
        int size = matrixWidth(matrix);
        double result = 0;
        for (int i = 0; i < size; i++) {
            double n = matrix[i][0];
            int val = (int) (n * Math.pow(-1, 2 + i));
            double[][] temp = removeRowAndColumn(i + 1, 1, matrix);
            result += determinant(temp) * val;
        }

        return result;
    }

    /**
     * Dodwania dwoch macierzy
     *
     * @param m1 macierz pierwsza
     * @param m2 macierz druga
     * @return wynik dodawania macierzy
     */
    public static double[][] add(double[][] m1, double[][] m2) {
        int width = matrixWidth(m1);
        int height = matrixHeight(m1);

        if (matrixWidth(m2) != width || matrixHeight(m2) != height) {
            return null;
        }

        double[][] result = new double[height][width];

        for (int i = 0; i < height; i++) {
            for (int n = 0; n < width; n++) {
                result[i][n] = m1[i][n] + m2[i][n];
            }
        }
        return result;
    }

    /**
     * Odejmowania od siebie macierzy
     *
     * @param m1 pierwsza macierz
     * @param m2 druga macierz
     * @return wynik odejmowania macierzy
     */
    public static double[][] subtract(double[][] m1, double[][] m2) {
        return add(m1, multiplyByValue(m2, -1));
    }

    /**
     * Mnozenie dwoch macierzy
     *
     * @param m1 macierz pierwsza
     * @param m2 macierz druga
     * @return wynik mnozenia macierzy
     */
    public static double[][] multiplyMatrices(double[][] m1, double[][] m2) {
        //Null jesli macierzy nie mozna przemnozyc
        if (!canMultiply(m1, m2)) {
            return null;
        }

        List<double[]> mAsList = new ArrayList<>();

        //Mnozenie macierzy w taki sposob:
        //Mnozenie osobno wszystkich wierszy pierwszej macierzy przez druga
        //Dodawania powstalych wierszy kolejno do listy
        //Po zakonczeniu macierz jest zapisana w postaci listy wierszy, ktora nalezy zamienic na tablice dwuelementowa
        for (int i = 0; i < matrixHeight(m1); i++) {
            int width = matrixWidth(m1);
            double[] row = new double[width];
            for (int n = 0; n < width; n++) {
                row[n] = m1[i][n];
            }
            mAsList.add(multiplyByRow(row, m2));
        }

        double[][] result = new double[mAsList.size()][matrixWidth(m2)];

        //Przerzucanie wartosci z listy wierszy macierzy, do tablicy dwuwymiarowej
        for (int i = 0; i < mAsList.size(); i++) {
            for (int n = 0; n < mAsList.get(0).length; n++) {
                result[i][n] = mAsList.get(i)[n];
            }
        }

        return result;
    }

    /**
     * Funkcja uzywana do mnozenia macierzy w {@link #multiplyMatrices(double[][], double[][])}, przemnaza podany wiersz macierzy, przez podana druga macierz
     *
     * @param row wiersz pierwszej macierzy
     * @param m   druga macierz
     * @return pierwszy wiersz przemnozony przez macierz
     */
    private static double[] multiplyByRow(double[] row, double[][] m) {
        int mWidth = matrixWidth(m);
        int rowLength = row.length;
        double[] result = new double[mWidth];

        for (int i = 0; i < mWidth; i++) {
            double temp = 0;
            for (int n = 0; n < rowLength; n++) {
                temp += row[n] * m[n][i];
            }
            result[i] = temp;
        }
        return result;
    }

    /**
     * Sprawdza, czy podane macierze mozna przez siebie pomnozyc
     *
     * @param m1 macierz pierwsza
     * @param m2 macierz druga
     * @return true jesli mozna pomnozyc, false jesli nie
     */
    private static boolean canMultiply(double[][] m1, double[][] m2) {
        return (matrixWidth(m1) == matrixHeight(m2));
    }

    /**
     * Mnozenie calej macierzy przez podana wartosc
     *
     * @param matrix macierz do przemnozenia
     * @param val    wartosc do przemnozenia macierzy
     * @return przemnozona macierz
     */
    public static double[][] multiplyByValue(double[][] matrix, int val) {
        int width = matrixWidth(matrix);
        int height = matrixHeight(matrix);
        double[][] result = new double[height][width];

        for (int i = 0; i < height; i++) {
            for (int n = 0; n < width; n++) {
                result[i][n] = val * matrix[i][n];
            }
        }

        return result;

    }

    /**
     * Funkcja wykorzystywana w {@link #Laplace(double[][])} usuwajaca podany wiersz i kolumne macierzy
     *
     * @param row    numer wiersza
     * @param column numer kolumny
     * @param matrix macierz do skrocenia
     * @return macierz matrix bez podanego wiersza i kolumny
     */
    private static double[][] removeRowAndColumn(int row, int column, double[][] matrix) {
        int size = matrixWidth(matrix) - 1;
        row--;
        column--;

        List<Integer> rows = new ArrayList<>();

        for (int i = 0; i < size + 1; i++) {
            if (i != row) {
                rows.add(i);
            }
        }
        List<Integer> columns = new ArrayList<>();
        for (int i = 0; i < size + 1; i++) {
            if (i != column) {
                columns.add(i);
            }
        }

        double[][] result = new double[size][size];

        for (int i = 0; i < size; i++) {
            for (int n = 0; n < size; n++) {
                result[i][n] = matrix[rows.get(i)][columns.get(n)];
            }
        }

        return result;
    }

    /**
     * Transponowanie macierzy
     *
     * @param matrix macierz do transponowania
     * @return Przetransponowana macierz
     */
    public static double[][] transpone(double[][] matrix) {
        int size = matrixWidth(matrix);
        double[][] result = new double[size][size];

        for (int i = 0; i < size; i++) {
            for (int n = 0; n < size; n++) {
                result[i][n] = matrix[n][i];
            }
        }
        return result;
    }

    /**
     * Liczenie szerokosci macierzy
     *
     * @param matrix Macierz do policzenia
     * @return szerokosc macierz
     */
    public static int matrixWidth(double[][] matrix) {
        int i = 0;

        //Odnoszenie sie do elementow pierwszego wiersza, az do napotkania bledu
        try {
            while (i++ >= 0) {
                double a = matrix[0][i];
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }

        return i;
    }

    /**
     * Liczenie wysokosci macierzy
     *
     * @param matrix Macierz do policzenia
     * @return wysokosc macierz
     */
    public static int matrixHeight(double[][] matrix) {
        int i = 0;

        //Odnoszenie sie do elementow pierszej kolumny, az do napotkania bledu
        try {
            while (i++ >= 0) {
                double a = matrix[i][0];
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }

        return i;

    }

    /**
     * Sprawdza czy macierz jest kwadratowa
     *
     * @param matrix macierz do sprawdzenia
     * @return true jesli jest kwadratowa, false jesli nie
     */
    private static boolean isSquareMatrix(double[][] matrix) {
        return (matrixWidth(matrix) == matrixHeight(matrix));
    }

    /**
     * Funkcja wypisujaca macierz na standardowe wyjscie
     *
     * @param matrix    macierz do wypissania
     * @param precision ilosc miejsc zerowych do wypisania przy elementach
     */
    public static void printMatrix(double[][] matrix, int precision) {
        StringBuilder stringBuilder = new StringBuilder();
        int width = matrixWidth(matrix);
        int height = matrixHeight(matrix);
        for (int i = 0; i < height; i++) {
            stringBuilder.append("|");
            for (int n = 0; n < width; n++) {
                stringBuilder.append(truncate(matrix[i][n], precision)).append(" ");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append("|").append("\n");
        }
        System.out.println(stringBuilder.toString());
    }

    /**
     * Funkcja obcinajaca ilosc niezerowych miejsc zmiennej double, do podanej ilosci
     *
     * @param val liczba do obciecia
     * @param p   ilosc miejsc po przecinku
     * @return liczba val z p iloscia miejsc po przecinku
     */
    private static double truncate(double val, int p) {
        double result = val * Math.pow(10, p);
        int temp = (int) result;
        return temp / Math.pow(10, p);
    }
}
