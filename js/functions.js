let heightInputFirstMatrix;
let widthInputFirstMatrix;
let heightInputSecondMatrix;
let widthInputSecondMatrix;
let cellSize = "60px";
let resultMatrix;

let firstMatrixHeightChanged = false;
let firstMatrixWidthChanged = false;

let secondMatrixHeightChanged = false;
let secondMatrixWidthChanged = false


let effect = function (dims) {

    let allocate = function (dims) {

        if (dims.length == 0) {
            return 0;
        } else {
            let array = [];

            for (let i = 0; i < dims[0]; i++) {

                array.push(allocate(dims.slice(1)));
            }

            return array;
        }
    };

    return allocate(dims);
};

function onLoad() {

    showSecondMatrix();

    heightInputFirstMatrix = document.getElementById("heightInputFirstMatrix");
    widthInputFirstMatrix = document.getElementById("widthInputFirstMatrix");

    heightInputSecondMatrix = document.getElementById("heightInputSecondMatrix");
    widthInputSecondMatrix = document.getElementById("widthInputSecondMatrix");
    resultMatrix = document.getElementById("resultMatrix");

}

function showSecondMatrix() {

    let checkbox = document.getElementById("secondMatrix");
    let secondMatrixDiv = document.getElementById("matrixInputSecond");
    let showMatrixButton = document.getElementById("showMatrixButton");

    if (checkbox.checked === true) {

        secondMatrixDiv.style.display = "block";
        showMatrixButton.textContent = "Create matrices";

        setChildrenDisabled("singleMatrixButtons", true);
        setChildrenDisabled("twoMatricesButtons", false);

    } else {

        removeElement("inputMatrixSecond");
        setChildrenDisabled("twoMatricesButtons", true);
        setChildrenDisabled("singleMatrixButtons", false);

        secondMatrixDiv.style.display = "none"
        showMatrixButton.textContent = "Create matrix";
    }
}

function setChildrenDisabled(id, disabled) {
    let parent = document.getElementById(id);
    let children = parent.childNodes;

    for (let i = 0; i < children.length; i++) {

        let child = children[i];
        child.disabled = disabled;

    }
}

function removeElement(id) {
    let element = document.getElementById(id);

    if (element)
        element.parentNode.removeChild(element);

}

function multiplyButtonClicked() {
    let matrix1 = readMatrix("inputMatrixFirst");
    let matrix2 = readMatrix("inputMatrixSecond");

    if (!canMultiply(matrix1, matrix2)) {
        alert("These matrices cannot be multiplied");
    } else {

        let resultMatrix = multiplyMatrices(matrix1, matrix2);

        resultMatrix.style.visibility = "initial";

        outputMatrix(resultMatrix, "display");
    }
}

function addButtonClicked() {
    let matrix1 = readMatrix("inputMatrixFirst");
    let matrix2 = readMatrix("inputMatrixSecond");


    if (matrixHeight(matrix1) === matrixHeight(matrix2) && matrixWidth(matrix1) === matrixWidth(matrix2) && isSquareMatrix(matrix1) && isSquareMatrix(matrix2)) {

        let resultMatrix = sumMatrices(matrix1, matrix2);

        resultMatrix.style.visibility = "initial";

        outputMatrix(resultMatrix, "display");

    } else {
        alert("These matrices cannot be summed.");
        return;
    }
}

function subtractButtonClicked() {
    let matrix1 = readMatrix("inputMatrixFirst");
    let matrix2 = readMatrix("inputMatrixSecond");

    if (matrixHeight(matrix1) === matrixHeight(matrix2) && matrixWidth(matrix1) === matrixWidth(matrix2) && isSquareMatrix(matrix1) && isSquareMatrix(matrix2)) {

        let resultMatrix = subtractMatrices(matrix1, matrix2);

        resultMatrix.style.visibility = "initial";

        outputMatrix(resultMatrix, "display");

    } else {
        alert("These matrices cannot be subtracted.");
        return;
    }
}

function outputMatrix(matrix, parent) {
    createTable(matrix, document.getElementById(parent));
}

function onFirstMatrixHeightChanged() {
    firstMatrixHeightChanged = true;
}

function onFirstMatrixWidthChanged() {
    firstMatrixWidthChanged = true;
}

function onSecondMatrixHeightChanged() {
    secondMatrixHeightChanged = true;
}

function onSecondMatrixWidthChanged() {
    secondMatrixWidthChanged = true;
}


function showMatrixButtonOnClick() {


    if (firstMatrixHeightChanged === true || secondMatrixWidthChanged === true) {

        showMatrix(widthInputFirstMatrix.value, heightInputFirstMatrix.value, "inputMatrixFirstDisplay", "inputMatrixFirst");

        firstMatrixHeightChanged = false;
        secondMatrixWidthChanged = false;

    }

    if (secondMatrixHeightChanged === true || secondMatrixWidthChanged === true) {

        showMatrixButtonOnClick2();

        secondMatrixHeightChanged = false;
        secondMatrixWidthChanged = false;

    }

}


function transposeButtonClicked() {

    let matrixBeforeTranspose = readMatrix("inputMatrixFirst");

    let heightMatrixBeforeTranspose = document.getElementById("heightInputFirstMatrix").value;
    let widthMatrixBeforeTranspose = document.getElementById("widthInputFirstMatrix").value;


    showMatrixTranspose(heightMatrixBeforeTranspose, widthMatrixBeforeTranspose, "inputMatrixFirstDisplay", "inputMatrixFirst", matrixBeforeTranspose);

    document.getElementById("heightInputFirstMatrix").value = widthMatrixBeforeTranspose;
    document.getElementById("widthInputFirstMatrix").value = heightMatrixBeforeTranspose;


}

function showMatrixButtonOnClick2() {

    width = widthInputSecondMatrix.value;
    height = heightInputSecondMatrix.value;


    showMatrix(width, height, "inputMatrixSecondDisplay", "inputMatrixSecond");
}

function evaluateMatrix(width, height) {
    return width > 0 && height > 0;
}

function readMatrix(id) {

    let table = document.getElementById(id);
    let matrix = [];


    for (let i = 0, row; row = table.rows[i]; i++) {

        let tempRow = [];

        for (let j = 0, col; col = row.cells[j]; j++) {

            let value = col.childNodes[0].value;
            tempRow.push(value);
        }

        matrix.push(tempRow);
    }
    return matrix;
}

function determinantButtonOnClick() {

    let alertMessage;
    let result = determinant(readMatrix("inputMatrixFirst"));

    if (result === "NaN") {

        alertMessage = "Only square matrices have determinants.";
    } else {
        alertMessage = "Determinant is " + result;
    }

    alert(alertMessage);
}

function createTable(matrix, parent) {


    removeElement("outputTable");
    let height = matrix.length;
    let width = matrix[0].length;
    let body = parent;
    let table = document.createElement('table');

    table.setAttribute("id", "outputTable");
    table.setAttribute('border', '1');

    let tbdy = document.createElement('tbody');

    for (let i = 0; i < height; i++) {
        let tr = document.createElement('tr');


        for (let j = 0; j < width; j++) {

            let td = document.createElement('td');

            td.style.width = cellSize;
            td.style.height = cellSize;

            let output = document.createElement("textarea");

            output.setAttribute("readonly", "true");
            output.setAttribute("type", "number");

            output.style.resize = "none";
            output.style.width = cellSize;
            output.style.height = cellSize;

            output.value = matrix[i][j];

            td.appendChild(output);
            tr.appendChild(td);

        }
        tbdy.appendChild(tr);
    }
    table.appendChild(tbdy);
    body.appendChild(table);
}

function showMatrixTranspose(width, height, parent, id, matrix) {
    evaluateMatrix(width, height) ? tableCreateTranspose(width, height, document.getElementById(parent), id, matrix) : alert("Invalid matrix dimensions");
}

function tableCreateTranspose(width, height, parent, id, matrix) {

    removeElement(id);

    let body = parent;
    let table = document.createElement('table');

    table.setAttribute("id", id);
    table.setAttribute('border', '1');

    let tbdy = document.createElement('tbody');

    for (let i = 0; i < width; i++) {
        let tr = document.createElement('tr');

        for (let j = 0; j < height; j++) {

            let td = document.createElement('td');

            td.style.width = cellSize;
            td.style.height = cellSize;

            let input = document.createElement("input");


            input.setAttribute("type", "number");

            input.style.width = cellSize;
            input.style.height = cellSize;

            input.value = matrix[j][i];

            td.appendChild(input);
            tr.appendChild(td);


        }

        tbdy.appendChild(tr);
    }
    table.appendChild(tbdy);

    body.appendChild(table)
}


function tableCreate(width, height, parent, id) {

    removeElement(id);

    let body = parent;
    let table = document.createElement('table');

    table.setAttribute("id", id);
    table.setAttribute('border', '1');

    let tbdy = document.createElement('tbody');

    for (let i = 0; i < width; i++) {
        let tr = document.createElement('tr');

        for (let j = 0; j < height; j++) {

            let td = document.createElement('td');

            td.style.width = cellSize;
            td.style.height = cellSize;

            let input = document.createElement("input");


            input.setAttribute("type", "number");
            input.style.width = cellSize;
            input.style.height = cellSize;


            td.appendChild(input);

            tr.appendChild(td);


        }

        tbdy.appendChild(tr);
    }
    table.appendChild(tbdy);

    body.appendChild(table)
}


function showMatrix(width, height, parent, id) {
    evaluateMatrix(width, height) ? tableCreate(width, height, document.getElementById(parent), id) : alert("Invalid matrix dimensions");
}

function isSquareMatrix(matrix) {
    return matrix.length === matrix[0].length;
}

function matrixWidth(matrix) {
    return matrix[0].length;
}

function matrixHeight(matrix) {
    return matrix.length;
}

function determinant(matrix) {

    if (!isSquareMatrix(matrix)) {
        return "NaN";
    }

    if (matrixHeight(matrix) > 3) {
        return Laplace(matrix);
    }

    if (matrixHeight(matrix) === 2) {
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }

    if (matrixHeight(matrix) === 1) {
        return matrix[0][0];
    }

    let left = 0;

    for (let n = 0; n < matrixHeight(matrix); n++) {
        {
            let temp = 1;

            for (let i = 0; i < matrixHeight(matrix); i++) {
                {
                    temp *= matrix[(i + n) % matrixHeight(matrix)][i % matrixHeight(matrix)];
                }
            }
            left += temp;
        }
    }
    let right = 0;

    for (let n = 0; n < matrixHeight(matrix); n++) {
        {
            let temp = 1;

            for (let i = 0; i < matrixHeight(matrix); i++) {
                {
                    temp *= matrix[(i) % matrixHeight(matrix)][(matrixHeight(matrix) - 1 - i - n + matrixHeight(matrix)) % matrixHeight(matrix)];
                }
            }
            right += temp;
        }
    }
    return left - right;
}

function Laplace(matrix) {

    let result = 0;

    for (let i = 0; i < matrixWidth(matrix); i++) {
        {
            let n = matrix[i][0];
            let val = ((n * Math.pow(-1, 2 + i)) | 0);
            let temp = removeRowAndColumn(i + 1, 1, matrix);

            result += determinant(temp) * val;
        }
    }

    return result;
}

function removeRowAndColumn(row, column, matrix) {

    let size = matrixWidth(matrix) - 1;
    row--;
    column--;
    let rows = ([]);

    for (let i = 0; i < size + 1; i++) {
        {
            if (i !== row) {
                (rows.push(i) > 0);
            }
        }
    }
    let columns = ([]);

    for (let i = 0; i < size + 1; i++) {
        {
            if (i !== column) {
                (columns.push(i) > 0);
            }
        }
    }
    let result = effect([size, size]);

    for (let i = 0; i < size; i++) {
        {
            for (let n = 0; n < size; n++) {
                {
                    result[i][n] = matrix[rows[i]][columns[n]];
                }
            }
        }

    }
    return result;
}

function sumMatrices(m1, m2) {

    if (matrixWidth(m2) !== matrixWidth(m1) || matrixHeight(m2) !== matrixHeight(m1)) {

        return null;
    }
    let result = effect([matrixHeight(m1), matrixWidth(m1)]);

    for (let i = 0; i < matrixHeight(m1); i++) {
        {
            for (let n = 0; n < matrixWidth(m1); n++) {
                {
                    result[i][n] = parseInt(m1[i][n], 10) + parseInt(m2[i][n], 10);
                }
            }
        }
    }
    return result;
}

function subtractMatrices(m1, m2) {

    return sumMatrices(m1, multiplyByValue(m2, -1));
}

function multiplyMatrices(m1, m2) {

    if (!canMultiply(m1, m2)) {

        return null;
    }
    let mAsList = ([]);

    for (let i = 0; i < matrixHeight(m1); i++) {
        {
            let row = (function (s) {

                let a = [];

                while (s-- > 0)

                    a.push(0);

                return a;

            })(matrixWidth(m1));

            for (let n = 0; n < matrixWidth(m1); n++) {
                {
                    row[n] = m1[i][n];
                }
            }
            (mAsList.push(multiplyByRow(row, m2)) > 0);
        }

    }
    let result = effect([mAsList.length, matrixWidth(m2)]);

    for (let i = 0; i < mAsList.length; i++) {
        {
            for (let n = 0; n < mAsList[0].length; n++) {
                {
                    result[i][n] = mAsList[i][n];
                }
            }
        }
    }
    return result;
}

function multiplyByRow(row, m) {
    let result = (function (s) {
        let a = [];

        while (s-- > 0)

            a.push(0);

        return a;

    })(matrixWidth(m));

    for (let i = 0; i < matrixWidth(m); i++) {
        {
            let temp = 0;

            for (let n = 0; n < row.length; n++) {
                {
                    temp += row[n] * m[n][i];
                }

            }

            result[i] = temp;
        }
    }
    return result;
}


function canMultiply(m1, m2) {

    return (matrixWidth(m1) === matrixHeight(m2));
}

function multiplyByValue(matrix, val) {

    let result = effect([matrixHeight(matrix), matrixWidth(matrix)]);

    for (let i = 0; i < matrixHeight(matrix); i++) {
        {
            for (let n = 0; n < matrixWidth(matrix); n++) {
                {
                    result[i][n] = val * matrix[i][n];
                }
            }
        }

    }
    return result;
}